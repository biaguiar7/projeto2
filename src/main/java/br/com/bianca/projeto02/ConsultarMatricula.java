package br.com.bianca.projeto02;

import java.util.Scanner;

import br.com.bianca.projeto02.dao.UsuarioDAO;

public class ConsultarMatricula {
	
	static Scanner ler = new Scanner(System.in);
	
	public static void consultarMatricula() {
		
		int matricula;
		System.out.println("Digite a matricula: ");
		matricula = ler.nextInt();
		ler.nextLine();

		Usuario usuario = UsuarioDAO.consultarMatricula(matricula);
		System.out.println("Nome: " + usuario.getNome());
		System.out.println("Turno de Treino: " + usuario.getTurnoDeTreino());
	}
	
}
