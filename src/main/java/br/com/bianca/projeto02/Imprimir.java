package br.com.bianca.projeto02;

import java.util.ArrayList;

import br.com.bianca.projeto02.dao.UsuarioDAO;

public class Imprimir {
	
	public static void imprimirLista() {
		 ArrayList<Usuario> usuarios = new UsuarioDAO().listarUsuarios();

		if(usuarios.isEmpty()) {
			System.out.println("Não há usuários cadastrados!");
			MenuInicial.main(null);
		}
		for (int i = 0; i < usuarios.size(); i++) {
			System.out.println("Nome: " + usuarios.get(i).getNome());
			System.out.println("Turno de Treino: " + usuarios.get(i).getTurnoDeTreino());
		}
	}
}
