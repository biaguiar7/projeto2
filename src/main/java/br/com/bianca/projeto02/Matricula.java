package br.com.bianca.projeto02;

import java.util.Random;

import javax.swing.JTextField;

public class Matricula {
	public static int geraMatricula() {
		Random mat = new Random();
		int matricula = mat.nextInt(99999);
		if (matricula < 0) {
			matricula *= (-1);
		}
		if (matricula <= 99999 || matricula > 99999) {
			return matricula;
		}else {
			return geraMatricula();
		}
		
}
 public static int getMatricula(JTextField matricula) {
	 try {
		 return((matricula.getText().charAt(0) - 48) * 10000) + ((matricula.getText().charAt(1)- 48 )* 1000) + ((matricula.getText().charAt(2)- 48 )* 100)
				 + ((matricula.getText().charAt(3)- 48 ) * 10) + ((matricula.getText().charAt(4)- 48 )* 1) ;
	 }catch (Exception e){
		 return 0;
	 }
 }


}

