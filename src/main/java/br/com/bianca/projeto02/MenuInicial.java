package br.com.bianca.projeto02;

import java.util.Scanner;

import br.com.bianca.projeto02.factory.Conexao;

public class MenuInicial {

	private static Scanner ler = new Scanner(System.in);

	public static void main(String[] args) {

		Conexao.conectar();
		while (true) {
			System.out.println("Digite: \n A)Cadastrar novo usu�rio\n B)Consultar matricula\n "
					+ "C)Exibir lista de pessoas matriculadas\n D)Deletar usuario\n E)Sair do sistema");

			String in = ler.nextLine();

			switch (in.toLowerCase()) {

			case "a":
				CriaUsuario.criaUsuario();
				break;
			case "b":
				ConsultarMatricula.consultarMatricula();
				break;

			case "c":
				Imprimir.imprimirLista();
				break;
			case "d":
				Excluir.deletarUsuario();
				break;
			case "e":

			default:
				Conexao.desconectarBanco();
				System.exit(0);
			}

		}

	}

}
