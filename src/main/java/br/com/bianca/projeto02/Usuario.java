package br.com.bianca.projeto02;


public class Usuario {

	private String nome;
	private String cpf;
	private String turnoDeTreino;
	private int matricula;
	
	public String getNome() {
		return nome;
	}

	public String getCpf() {
		return cpf;
	}

	public String getTurnoDeTreino() {
		return turnoDeTreino;
	}

	public int getMatricula() {
		return matricula;
	}

	public Usuario(String nome, String cpf, String turnoDeTreino ) {
		this.nome = nome;
		this.cpf = cpf;
		this.turnoDeTreino = turnoDeTreino;
		
		
	}
	
	public Usuario(int matricula, String nome, String cpf, String turnoDeTreino ) {
		this.matricula = matricula;
		this.nome = nome;
		this.cpf = cpf;
		this.turnoDeTreino = turnoDeTreino;
	}
}
