package br.com.bianca.projeto02.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import br.com.bianca.projeto02.Usuario;
import br.com.bianca.projeto02.factory.Conexao;

public class UsuarioDAO {

	public static void adicionar(Usuario usuario) {

		try {
			String sql = "insert into usuários(matricula, nome, cpf, turnoDeTreino) values (?,?,?,?)";
			PreparedStatement ps = Conexao.createConnection().prepareStatement(sql);
			
			ps.setInt(1, usuario.getMatricula());
			ps.setString(2, usuario.getNome());
			ps.setString(3, usuario.getCpf());
			ps.setString(4, usuario.getTurnoDeTreino());

			ps.execute();
			ps.close();
			sql = "Select matricula from usuários where cpf = " + usuario.getCpf();
			ps = Conexao.createConnection().prepareStatement(sql);

			ResultSet rs = ps.executeQuery(sql);
			rs.next();
//			System.out.println("Sua matrícula é : " + rs.getInt("matricula"));
			JOptionPane.showMessageDialog(null,
					"Usuário adicionado com sucesso!" + "\nSua matrícula é : " + rs.getInt("matricula"));
			ps.close();
			// System.out.println("Usuário adicionado com sucesso!");

		} catch (SQLException e) {
			System.out.println("Erro ao adicionar usuário!" + e);
		}

	}

	public static void consultarMatricula(int matricula) {

		try {
			String sql = "Select matricula, nome, cpf, turnoDeTreino from usuários where matricula = " + matricula;
			PreparedStatement ps1 = Conexao.createConnection().prepareStatement(sql);

			ResultSet rs = ps1.executeQuery(sql);

			if (rs.next()) {
				int matricula1 = rs.getInt("matricula");
				String nome = rs.getString("nome");
				String turnoDeTreino = rs.getString("turnoDeTreino");
				String cpf = rs.getString("cpf");
				JOptionPane.showMessageDialog(null, "Nome: " + nome + "\nTurno: " + turnoDeTreino + "\nMatricula: " + matricula) ;
							}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public static ArrayList<Usuario> listarUsuarios() {

		try {
			String sql = "Select * from usuários order by nome";
			PreparedStatement ps1 = Conexao.createConnection().prepareStatement(sql);

			ResultSet rs = ps1.executeQuery(sql);
			ArrayList<Usuario> usuarios = new ArrayList<>();

			while (rs.next()) {
				usuarios.add(new Usuario(rs.getInt("matricula"), rs.getString("nome"), rs.getString("cpf"), rs.getString("turnoDeTreino")));
			}
			return usuarios;

		} catch (SQLException e) {
			throw new RuntimeException(e);

		}
	}

	public static void deletar(int matricula) {

		try {
			String sql = "Delete from usuários where matricula = " + matricula;
			PreparedStatement ps = Conexao.createConnection().prepareStatement(sql);
			ps.execute();
			ps.close();
		} catch (SQLException e) {
			throw new RuntimeException("Não foi possível deletar usuário!" + e);
		}
	}
}
