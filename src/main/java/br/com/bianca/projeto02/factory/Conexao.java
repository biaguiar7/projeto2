package br.com.bianca.projeto02.factory;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.JOptionPane;

public class Conexao {

	private  static Connection cnn;
	
	public static void conectar() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/projeto02", "root", "");
		} catch (SQLException | ClassNotFoundException e) {
			throw new RuntimeException("Erro na conexão com o Banco de Dados" + e);
		}
	}
	
	public static void desconectarBanco() {
		try {
			if (cnn !=null) {
				cnn.close();
				JOptionPane.showMessageDialog (null, "A desconexão com o banco foi realizada");
			}
			} catch(SQLException e ){
				JOptionPane.showMessageDialog(null, "Não foi possivel desconectar do banco de dados" );
			}
			
		}
	public static Connection createConnection() {
		return cnn;
	}
	
	
	

}
