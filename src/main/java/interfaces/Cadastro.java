package interfaces;

import java.awt.Choice;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import br.com.bianca.projeto02.CriaUsuario;
import br.com.bianca.projeto02.Matricula;
import br.com.bianca.projeto02.ValidarCPF;

public class Cadastro extends JFrame {
	private String turno;
	private JPanel contentPane;
	private JTextField nome;
	private JTextField cpf;
	
	
	public Cadastro() {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Digite seus dados:");
		lblNewLabel.setBounds(162, 24, 105, 14);
		contentPane.add(lblNewLabel);
		
		final Choice choice = new Choice();
		choice.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (choice.getSelectedItem() == "Selecione") {
					JOptionPane.showMessageDialog(null, "Selecione um turno");
				}else {
					turno=choice.getSelectedItem();
				}
 			}
		});
		choice.add("Selecione");
		choice.add("Manhã");
		choice.add("Tarde");
		choice.add("Noite");
		
		choice.setBounds(146, 139, 129, 20);
		contentPane.add(choice);
		
		JLabel lblNewLabel_1 = new JLabel("Nome: ");
		lblNewLabel_1.setBounds(43, 53, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		nome = new JTextField();
		nome.setBounds(126, 49, 209, 20);
		contentPane.add(nome);
		nome.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("CPF:");
		lblNewLabel_2.setBounds(43, 78, 46, 14);
		contentPane.add(lblNewLabel_2);
		
		cpf = new JTextField();
		cpf.setBounds(126, 75, 165, 20);
		contentPane.add(cpf);
		cpf.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Selecione um turno de treino");
		lblNewLabel_3.setBounds(146, 106, 165, 40);
		contentPane.add(lblNewLabel_3);
		
		JButton btnNewButton = new JButton("Cadastrar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (nome.getText().isEmpty() || cpf.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Preencha todos os campos");
					new Cadastro();
					
				}else if (ValidarCPF.Validar(cpf.getText())) {
					CriaUsuario.criaUsuario(Matricula.geraMatricula(), nome.getText(), cpf.getText(), turno);
				}
				
				dispose();
				
			}
		});
		
		btnNewButton.setBounds(162, 177, 111, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Voltar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PainelInicial();
				dispose();
			}
		});
		btnNewButton_1.setBounds(177, 227, 89, 23);
		contentPane.add(btnNewButton_1);
		
	}
}
