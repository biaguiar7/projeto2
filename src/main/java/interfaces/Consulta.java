package interfaces;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.bianca.projeto02.Matricula;
import br.com.bianca.projeto02.dao.UsuarioDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Consulta extends JFrame {

	private JPanel contentPane;
	private JTextField matricula;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Consulta() {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {

					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblNewLabel = new JLabel("Informe a matrícula");
		lblNewLabel.setBounds(162, 35, 127, 14);
		contentPane.add(lblNewLabel);

		matricula = new JTextField();
		matricula.setBounds(172, 60, 86, 20);
		contentPane.add(matricula);
		matricula.setColumns(10);

		JButton btnNewButton = new JButton("Consultar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (matricula.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Digite uma matrícula");
				} else {
					UsuarioDAO.consultarMatricula(Matricula.getMatricula(matricula));
				}

			}
		});
		btnNewButton.setBounds(172, 108, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Voltar");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PainelInicial();
				dispose();
			}
		});
		btnNewButton_1.setBounds(178, 227, 89, 23);
		contentPane.add(btnNewButton_1);
	}
}
