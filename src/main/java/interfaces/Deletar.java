package interfaces;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import br.com.bianca.projeto02.Excluir;
import br.com.bianca.projeto02.Matricula;
import br.com.bianca.projeto02.dao.UsuarioDAO;

public class Deletar extends JFrame {

	private JPanel contentPane;
	
	private JTextField matricula;

	
	public Deletar() {
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		
		JLabel lblNewLabel = new JLabel("Digite a matrícula:");
		lblNewLabel.setBounds(168, 30, 104, 20);
		contentPane.add(lblNewLabel);
		
		
		
		matricula = new JTextField();
		matricula.setBounds(161, 61, 111, 20);
		contentPane.add(matricula);
		matricula.setColumns(10);
		
		JButton btnNewButton = new JButton("Excluir");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			if (matricula.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Digite uma matrícula");
			}else {
				Excluir.deletarUsuario(Matricula.getMatricula(matricula));
			}
			}
		});
		btnNewButton.setBounds(168, 109, 89, 23);
		contentPane.add(btnNewButton);
		
		JButton Voltar = new JButton("Voltar");
		Voltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PainelInicial();
				dispose();
			}
		});
		Voltar.setBounds(161, 211, 121, 23);
		contentPane.add(Voltar);
		
		
	}
}
