package interfaces;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import br.com.bianca.projeto02.Usuario;
import br.com.bianca.projeto02.dao.UsuarioDAO;

public class Listar extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JButton voltar;
	private JButton Voltar;

	public Listar() {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(600,500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		table = new JTable();
		table.setCellSelectionEnabled(true);
		table.setColumnSelectionAllowed(true);;
		table.setBounds(21, 11, 541, 257);
		getContentPane().add(table);
		
		JLabel lblNewLabel = new JLabel("Clientes:");
		lblNewLabel.setBounds(188, 11, 46, 14);
		contentPane.add(lblNewLabel);
		
		voltar = new JButton("Voltar");
		voltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PainelInicial();
				dispose();
			}
		});
		voltar.setBounds(175, 227, 89, 23);
		contentPane.add(voltar);
		
		Voltar = new JButton("Voltar");
		Voltar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new PainelInicial();
				dispose();
				}
		});
		Voltar.setBounds(236, 308, 89, 23);
		contentPane.add(Voltar);
		Lista();
		
	}
	public void Lista() {
		DefaultTableModel model = (DefaultTableModel) table.getModel();
		model.addColumn("Nome");
		model.addColumn("Turno");
		model.addColumn("Matricula");
		model.setNumRows(0);
		model.addRow(new Object[] {"Nome:", "Turno de treino:", "Matrícula"});
		
		
		table.getColumnModel().getColumn(0).setPreferredWidth(50);
		table.getColumnModel().getColumn(1).setPreferredWidth(25);
		table.getColumnModel().getColumn(2).setPreferredWidth(25);
		
		
		for (Usuario usuario : UsuarioDAO.listarUsuarios()) {
			model.addRow(new Object[] {usuario.getNome(), usuario.getTurnoDeTreino(), usuario.getMatricula()});
		}
	}
}
