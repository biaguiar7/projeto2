package interfaces;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import br.com.bianca.projeto02.factory.Conexao;

public class PainelInicial extends JFrame {
	private JPanel contentPane;

	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PainelInicial frame = new PainelInicial();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	
	public PainelInicial() {
		Conexao.conectar();
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Escolha uma opção:");
		lblNewLabel.setBounds(163, 25, 133, 23);
		contentPane.add(lblNewLabel);
		
		JButton btnNewButton = new JButton("Cadastrar Aluno");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Cadastro();
				dispose();
			}
		});
		btnNewButton.setBounds(23, 59, 154, 23);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Consultar Matrícula");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Consulta();
				dispose();
			}
		});
		btnNewButton_1.setBounds(270, 59, 154, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_3 = new JButton("Sair");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				Conexao.desconectarBanco();
			}
		});
		btnNewButton_3.setBounds(175, 210, 89, 23);
		contentPane.add(btnNewButton_3);
		
		JButton btnNewButton_2 = new JButton("Deletar aluno");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Deletar();
				dispose();
			}
		});
		btnNewButton_2.setBounds(270, 110, 154, 23);
		contentPane.add(btnNewButton_2);
		
		JButton btnNewButton_4 = new JButton("Listar alunos");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Listar();
				dispose();
			}
		});
		btnNewButton_4.setBounds(23, 110, 154, 23);
		contentPane.add(btnNewButton_4);
	}
}
